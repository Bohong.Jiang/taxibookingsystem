using NUnit.Framework;
using TaxiBookingSystem.Services;
using TaxiBookingSystem.Services.Dtos;

namespace TaxiBookingSystem.UnitTest
{
    public class Tests
    {
        private ITaxiBookingService? taxiBookingService;

        [SetUp]
        public void Setup()
        {
            taxiBookingService = TaxiBookingService.Instance;
            taxiBookingService.Reset();
        }

        [Test]
        public void Book_Three_Times_Returns_Cars_In_Sequence()
        {
            // Arrange

            // Act
            BookingResponseDto? response1 = taxiBookingService.Book(new MapLocationDto(1, 2), new MapLocationDto(3, 4));
            BookingResponseDto? response2 = taxiBookingService.Book(new MapLocationDto(1, 2), new MapLocationDto(-3, -4));
            BookingResponseDto? response3 = taxiBookingService.Book(new MapLocationDto(-1, -2), new MapLocationDto(-3, -4));

            // Assert
            Assert.AreEqual(1, response1.CarId);
            Assert.AreEqual(7, response1.TotalTime);
            Assert.AreEqual(2, response2.CarId);
            Assert.AreEqual(13, response2.TotalTime);
            Assert.AreEqual(3, response3.CarId);
            Assert.AreEqual(7, response3.TotalTime);
        }

        [Test]
        public void Book_When_No_Car_Available_Returns_Null()
        {
            // Arrange

            // Act
            BookingResponseDto? response1 = taxiBookingService.Book(new MapLocationDto(0, 0), new MapLocationDto(1, 1));
            BookingResponseDto? response2 = taxiBookingService.Book(new MapLocationDto(0, 0), new MapLocationDto(-2, -2));
            BookingResponseDto? response3 = taxiBookingService.Book(new MapLocationDto(0, 0), new MapLocationDto(-3, 3));
            BookingResponseDto? response4 = taxiBookingService.Book(new MapLocationDto(0, 0), new MapLocationDto(4, -4));

            // Assert
            Assert.AreEqual(1, response1.CarId);
            Assert.AreEqual(2, response1.TotalTime);
            Assert.AreEqual(2, response2.CarId);
            Assert.AreEqual(4, response2.TotalTime);
            Assert.AreEqual(3, response3.CarId);
            Assert.AreEqual(6, response3.TotalTime);
            Assert.IsNull(response4);
        }

        [Test]
        public void Book_After_Reset_Returns_Car1()
        {
            // Arrange
            MapLocationDto source = new MapLocationDto(1, 1);
            MapLocationDto destination = new MapLocationDto(2, 2);
            BookingResponseDto? response = taxiBookingService.Book(source, destination);
            Assert.AreEqual(1, response.CarId);
            response = taxiBookingService.Book(source, destination);
            Assert.AreEqual(2, response.CarId);

            // Act
            taxiBookingService.Reset();
            response = taxiBookingService.Book(source, destination);

            // Assert
            Assert.AreEqual(1, response.CarId);
        }

        [Test]
        public void Book_Car1_Then_Reach_Destination_Then_Book_Again_Returns_Car1()
        {
            // Arrange
            MapLocationDto source = new MapLocationDto(2, 2);
            MapLocationDto destination = new MapLocationDto(3, 3);
            BookingResponseDto? response = taxiBookingService.Book(source, destination);
            Assert.AreEqual(1, response.CarId);
            Assert.AreEqual(6, response.TotalTime);

            // Act
            for (int i = 0; i < 6; i++)
            {
                taxiBookingService.Tick();
            }

            // Assert
            response = taxiBookingService.Book(source, destination);
            Assert.AreEqual(1, response.CarId);
            Assert.AreEqual(4, response.TotalTime);
        }

        [Test]
        public void Book_Car1_Then_Before_Reach_Destination_Then_Book_Again_Returns_Car2()
        {
            // Arrange
            MapLocationDto source = new MapLocationDto(2, 2);
            MapLocationDto destination = new MapLocationDto(3, 3);
            BookingResponseDto? response = taxiBookingService.Book(source, destination);
            Assert.AreEqual(1, response.CarId);
            Assert.AreEqual(6, response.TotalTime);

            // Act
            for (int i = 0; i < 5; i++)
            {
                taxiBookingService.Tick();
            }

            // Assert
            response = taxiBookingService.Book(source, destination);
            Assert.AreEqual(2, response.CarId);
            Assert.AreEqual(6, response.TotalTime);
        }

        [Test]
        public void Book_Car_At_Random_Sequence_Returns_Expected_Cars()
        {
            // Book 3 times at the same time
            BookingResponseDto? response = taxiBookingService.Book(new MapLocationDto(0, 0), new MapLocationDto(1, 1));
            Assert.AreEqual(1, response.CarId);
            Assert.AreEqual(2, response.TotalTime);
            response = taxiBookingService.Book(new MapLocationDto(0, 0), new MapLocationDto(1, 1));
            Assert.AreEqual(2, response.CarId);
            Assert.AreEqual(2, response.TotalTime);
            response = taxiBookingService.Book(new MapLocationDto(0, 0), new MapLocationDto(1, 1));
            Assert.AreEqual(3, response.CarId);
            Assert.AreEqual(2, response.TotalTime);
            // All cars reached destination
            for (int i = 0; i < 2; i++)
            {
                taxiBookingService.Tick();
            }

            // Book 3 times at the same time
            response = taxiBookingService.Book(new MapLocationDto(1, 1), new MapLocationDto(2, 2));
            Assert.AreEqual(1, response.CarId);
            Assert.AreEqual(2, response.TotalTime);
            response = taxiBookingService.Book(new MapLocationDto(2, 2), new MapLocationDto(3, 3));
            Assert.AreEqual(2, response.CarId);
            Assert.AreEqual(4, response.TotalTime);
            response = taxiBookingService.Book(new MapLocationDto(0, 0), new MapLocationDto(-1, -1));
            Assert.AreEqual(3, response.CarId);
            Assert.AreEqual(4, response.TotalTime);
            // Only car1 reached it's destination
            for (int i = 0; i < 3; i++)
            {
                taxiBookingService.Tick();
            }

            // Book again
            response = taxiBookingService.Book(new MapLocationDto(-1, -1), new MapLocationDto(-2, -2));
            Assert.AreEqual(1, response.CarId);
            Assert.AreEqual(8, response.TotalTime);
            // Car2 (3, 3) & 3 (-1, -1) reached previous booking destination
            taxiBookingService.Tick();

            // Book again should return nearest car
            response = taxiBookingService.Book(new MapLocationDto(4, 4), new MapLocationDto(5, 5));
            Assert.AreEqual(2, response.CarId);
            Assert.AreEqual(4, response.TotalTime);
            response = taxiBookingService.Book(new MapLocationDto(0, 0), new MapLocationDto(-3, -3));
            Assert.AreEqual(3, response.CarId);
            Assert.AreEqual(8, response.TotalTime);
        }
    }
}