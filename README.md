# Instructions to run this test application

## Environment requirement
- Windows/Linux with .NET 6 framework installed

## Get source code from gitlab repo
- git clone https://gitlab.com/Bohong.Jiang/taxibookingsystem.git

## Run application
### Run application from source code
- Go to application dir. (./TaxiBookingSystem)
- Execute command: "cd TaxiBookingSystem.Application"
- Execute command: "dotnet run"
### Run application from docker container
- Go to application dir. (./TaxiBookingSystem)
- Execute command to build docker image: "docker build -t taxibooking-img ."
- Execute command to start docker container: "docker run -d -p 8080:80 --name mytaxibookingapp taxibooking-img"

## Run unit tests
- Go to application dir. (./TaxiBookingSystem)
- Execute command: cd TaxiBookingSystem.UnitTest
- Execute command: dotnet test




# APIs
## "POST /api/book"
- Request payload: (json)
{
  "source": {
    "x": x1,
    "y": y1
  },
  "destination": {
    "x": x2,
    "y": y2
  }
}
- HttpStatus: 200 (OK)
- Response payload: (json)
{
  "car_id": id,
  "total_time": t
}

## "POST /api/tick"
- Request payload: N.A
- HttpStatus: 204 (No Content)
- Response payload: N.A

## "POST /api/reset"
- Request payload: N.A
- HttpStatus: 204 (No Content)
- Response payload: N.A