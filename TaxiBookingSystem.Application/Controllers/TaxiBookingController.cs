using Microsoft.AspNetCore.Mvc;
using TaxiBookingSystem.Mappers;
using TaxiBookingSystem.Models;
using TaxiBookingSystem.Services;

namespace TaxiBookingSystem.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TaxiBookingController : ControllerBase
    {
        [HttpPost()]
        [Route("/api/book")]
        public IActionResult BookTaxi(BookingRequest request)
        {
            BookingResponse? response = DtoMapper.BookingResponseDtoToBookingResponse(
                                            TaxiBookingService.Instance.Book(
                                                DtoMapper.MapLocationToMapLocationDto(request.source),
                                                DtoMapper.MapLocationToMapLocationDto(request.destination))
                                            );
            if (response == null)
                return NotFound();

            return Ok(response);
        }

        [HttpPost()]
        [Route("/api/reset")]
        public IActionResult Reset()
        {
            TaxiBookingService.Instance.Reset();
            return NoContent();
        }

        [HttpPut()]
        [Route("/api/tick")]
        public IActionResult Tick()
        {
            TaxiBookingService.Instance.Tick();
            return NoContent();
        }
    }
}