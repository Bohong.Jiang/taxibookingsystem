﻿using TaxiBookingSystem.Models;
using TaxiBookingSystem.Services.Dtos;

namespace TaxiBookingSystem.Mappers
{
    /// <summary>
    /// Map data between controller models and service dtos.
    /// </summary>
    public class DtoMapper
    {
        public static MapLocationDto MapLocationToMapLocationDto(MapLocation mapLocation)
        {
            if (mapLocation == null) 
                throw new ArgumentNullException(nameof(mapLocation));

            return new MapLocationDto(mapLocation.X, mapLocation.Y);
        }

        public static BookingResponse? BookingResponseDtoToBookingResponse(BookingResponseDto bookingResponseDto)
        {
            if (bookingResponseDto == null)
                return null;

            return new BookingResponse()
            {
                car_id = bookingResponseDto.CarId,
                total_time = bookingResponseDto.TotalTime
            };
        }
    }
}
