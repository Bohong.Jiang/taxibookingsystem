﻿using TaxiBookingSystem.Services.Dtos;

namespace TaxiBookingSystem.Services
{
    public interface ITaxiBookingService
    {
        /// <summary>
        /// Picks the nearest available car to the customer location and return the total time taken to travel from 
        /// the current car location to customer location then to customer destination.
        /// </summary>
        /// <param name="source"> Source location where user starts. </param>
        /// <param name="destination"> Destination location where user is going to. </param>
        /// <returns>Booking response.</returns>
        BookingResponseDto? Book(MapLocationDto source, MapLocationDto destination);

        /// <summary>
        /// When called should advance your service time stamp by 1 time unit.
        /// </summary>
        void Tick();

        /// <summary>
        /// When called will reset all cars data back to the initial state regardless of cars that are currently booked.
        /// </summary>
        void Reset();
    }
}
