﻿using TaxiBookingSystem.Models;

namespace TaxiBookingSystem.Services.Dtos
{
    public class MapLocationDto
    {
        public MapLocationDto(int x, int y)
        {
            X = x;
            Y = y;
        }
        public int X { get; set; }
        public int Y { get; set; }
    }
}
