﻿namespace TaxiBookingSystem.Services.Dtos
{
    public class BookingResponseDto
    {
        public int CarId { get; set; }
        public int TotalTime { get; set; }
    }
}
