﻿using TaxiBookingSystem.Services.Dtos;

namespace TaxiBookingSystem.Services.Dtos
{
    public class TaxiDto
    {
        public int Id { get; set; }
        public MapLocationDto? CurrentLocation { get; set; }
        public MapLocationDto? DestinationLocation { get; set; }
        public bool IsBooked => DestinationLocation != null; 

    }
}
