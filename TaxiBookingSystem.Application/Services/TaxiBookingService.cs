﻿using TaxiBookingSystem.Models;
using TaxiBookingSystem.Services.Dtos;

namespace TaxiBookingSystem.Services
{
    /// <summary>
    /// Impliement as singleton class
    /// </summary>
    public class TaxiBookingService : ITaxiBookingService
    {
        private static TaxiBookingService? _instance;
        public static TaxiBookingService Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new TaxiBookingService();

                return _instance;
            }
        }

        private List<TaxiDto> Taxis = new List<TaxiDto>();
        
        private TaxiBookingService()
        {
            InitTaxis();
        }

        public BookingResponseDto? Book(MapLocationDto source, MapLocationDto destination)
        {
            TaxiDto? taxiToBook = null;
            foreach (TaxiDto taxi in Taxis)
            {
                if (taxi.IsBooked)
                    continue;

                if (taxiToBook == null || GetDistanceBetweenTwoLocation(taxi.CurrentLocation, source) < GetDistanceBetweenTwoLocation(taxiToBook.CurrentLocation, source))
                    taxiToBook = taxi;
            }

            // No taxi available
            if (taxiToBook == null)
                return null;

            // Find a taxi
            taxiToBook.DestinationLocation = destination;
            return new BookingResponseDto()
            {
                CarId = taxiToBook.Id,
                TotalTime = GetDistanceBetweenTwoLocation(taxiToBook.CurrentLocation, source) + GetDistanceBetweenTwoLocation(source, destination)
            };
        }

        public void Reset()
        {
            InitTaxis();
        }

        public void Tick()
        {
            foreach (TaxiDto taxi in Taxis)
            {
                if (!taxi.IsBooked)
                    continue;

                MoveTaxiByOneTick(taxi);
                
                // Reset destination up on reach
                if (taxi.CurrentLocation.X == taxi.DestinationLocation.X &&
                    taxi.CurrentLocation.Y == taxi.DestinationLocation.Y)
                {
                    taxi.DestinationLocation = null;
                }
            }
        }

        /// <summary>
        /// Calculate distance between 2 map locations.
        /// </summary>
        /// <param name="location1"></param>
        /// <param name="location2"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        private int GetDistanceBetweenTwoLocation(MapLocationDto location1, MapLocationDto location2)
        {
            if (location1 == null || location2 == null)
                throw new ArgumentNullException("location");

            return Math.Abs(location1.X - location2.X) + Math.Abs(location1.Y - location2.Y);
        }

        /// <summary>
        /// Move taxi by 1 step. X 1st then Y.
        /// </summary>
        /// <param name="taxi"></param>
        /// <exception cref="ArgumentNullException"></exception>
        private void MoveTaxiByOneTick(TaxiDto taxi)
        {
            if (taxi == null)
                throw new ArgumentNullException("taxi");
            if (taxi.DestinationLocation == null)
                return;

            if (taxi.CurrentLocation.X > taxi.DestinationLocation.X)
                taxi.CurrentLocation.X--;
            else if (taxi.CurrentLocation.X < taxi.DestinationLocation.X)
                taxi.CurrentLocation.X++;
            else if (taxi.CurrentLocation.Y > taxi.DestinationLocation.Y)
                taxi.CurrentLocation.Y--;
            else if (taxi.CurrentLocation.Y < taxi.DestinationLocation.Y)
                taxi.CurrentLocation.Y++;
        }

        private void InitTaxis()
        {
            Taxis.Clear();
            Taxis.Add(new TaxiDto() { Id = 1, CurrentLocation = new MapLocationDto(0, 0) });
            Taxis.Add(new TaxiDto() { Id = 2, CurrentLocation = new MapLocationDto(0, 0) });
            Taxis.Add(new TaxiDto() { Id = 3, CurrentLocation = new MapLocationDto(0, 0) });
        }
    }
}
