﻿namespace TaxiBookingSystem.Models
{
    public class BookingResponse
    {
        public int car_id { get; set; }
        public int total_time { get; set; }
    }
}
