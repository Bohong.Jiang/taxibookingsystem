﻿namespace TaxiBookingSystem.Models
{
    public class BookingRequest
    {
        public MapLocation? source { get; set; }
        public MapLocation? destination { get; set; }
    }
}
