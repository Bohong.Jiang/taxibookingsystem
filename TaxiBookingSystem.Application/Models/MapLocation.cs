namespace TaxiBookingSystem.Models
{
    public class MapLocation
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}